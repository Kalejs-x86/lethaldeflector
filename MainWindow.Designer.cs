﻿namespace LethalDeflector_CSharp
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.processFoundLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InGameLbl = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BallWindupLbl = new System.Windows.Forms.Label();
            this.AutoFaceChk = new System.Windows.Forms.CheckBox();
            this.CarryBallChk = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lethal League process:";
            // 
            // processFoundLbl
            // 
            this.processFoundLbl.AutoSize = true;
            this.processFoundLbl.Location = new System.Drawing.Point(136, 9);
            this.processFoundLbl.Name = "processFoundLbl";
            this.processFoundLbl.Size = new System.Drawing.Size(54, 13);
            this.processFoundLbl.TabIndex = 0;
            this.processFoundLbl.Text = "Not found";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "In game:";
            // 
            // InGameLbl
            // 
            this.InGameLbl.AutoSize = true;
            this.InGameLbl.Location = new System.Drawing.Point(136, 32);
            this.InGameLbl.Name = "InGameLbl";
            this.InGameLbl.Size = new System.Drawing.Size(32, 13);
            this.InGameLbl.TabIndex = 0;
            this.InGameLbl.Text = "False";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.BallWindupLbl);
            this.groupBox1.Location = new System.Drawing.Point(12, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 79);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Debug";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ball windup:";
            // 
            // BallWindupLbl
            // 
            this.BallWindupLbl.AutoSize = true;
            this.BallWindupLbl.Location = new System.Drawing.Point(86, 16);
            this.BallWindupLbl.Name = "BallWindupLbl";
            this.BallWindupLbl.Size = new System.Drawing.Size(13, 13);
            this.BallWindupLbl.TabIndex = 0;
            this.BallWindupLbl.Text = "0";
            // 
            // AutoFaceChk
            // 
            this.AutoFaceChk.AutoSize = true;
            this.AutoFaceChk.Location = new System.Drawing.Point(21, 53);
            this.AutoFaceChk.Name = "AutoFaceChk";
            this.AutoFaceChk.Size = new System.Drawing.Size(69, 17);
            this.AutoFaceChk.TabIndex = 2;
            this.AutoFaceChk.Text = "Autoface";
            this.AutoFaceChk.UseVisualStyleBackColor = true;
            // 
            // CarryBallChk
            // 
            this.CarryBallChk.AutoSize = true;
            this.CarryBallChk.Location = new System.Drawing.Point(96, 53);
            this.CarryBallChk.Name = "CarryBallChk";
            this.CarryBallChk.Size = new System.Drawing.Size(69, 17);
            this.CarryBallChk.TabIndex = 2;
            this.CarryBallChk.Text = "Carry ball";
            this.CarryBallChk.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 170);
            this.Controls.Add(this.CarryBallChk);
            this.Controls.Add(this.AutoFaceChk);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.InGameLbl);
            this.Controls.Add(this.processFoundLbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainWindow";
            this.Text = "LethalDeflector";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label processFoundLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label InGameLbl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label BallWindupLbl;
        private System.Windows.Forms.CheckBox AutoFaceChk;
        private System.Windows.Forms.CheckBox CarryBallChk;
    }
}

