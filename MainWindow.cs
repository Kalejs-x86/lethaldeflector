﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace LethalDeflector_CSharp
{

    public partial class MainWindow : Form
    {
        // Settings
        const int BALL_RANGE_RADIUS = 8;

        const Int32 COORDINATE_STEP = 1179360;

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesWritten);

        [DllImport("User32.dll")]
        public static extern IntPtr GetForegroundWindow();

        bool ProcessFound = false;
        Process TargetProcess = null;
        static IntPtr ProcessHandle = IntPtr.Zero;

        Thread MainThread;
        Thread SearcherThread;

        public static Int32 NormalizeCoordinate(Int32 Val)
        {
            return Val / COORDINATE_STEP;
        }

        public static Int32 DenormalizeCoordinate(Int32 Val)
        {
            return Val * COORDINATE_STEP;
        }

        public static Int32 DerefPointers(Int32 BaseAddress, List<Int32> Offsets)
        {
            const int BufferSize = sizeof(Int32);
            byte[] ByteBuffer = new byte[BufferSize];
            int BytesRead = 0;

            Int32 CurAddress = BaseAddress;

            for (int i = 0; i < Offsets.Count; ++i)
            {
                Int32 Offset = Offsets[i];
                if (i != Offsets.Count - 1)
                {
                    ReadProcessMemory((IntPtr)ProcessHandle, (IntPtr)(CurAddress + Offset), ByteBuffer, BufferSize, ref BytesRead);
                    if (BytesRead == BufferSize)
                    {
                        CurAddress = BitConverter.ToInt32(ByteBuffer, 0);
                    }
                    else
                    {
                        throw new Exception("Failed to read memory");
                    }
                }
                else
                    CurAddress += Offset;
            }

            return CurAddress;
        }

        public static Int32 ReadMemory(Int32 Address)
        {
            const int BufferSize = sizeof(Int32);
            byte[] ByteBuffer = new byte[BufferSize];
            int BytesRead = 0;

            ReadProcessMemory((IntPtr)ProcessHandle, (IntPtr)Address, ByteBuffer, BufferSize, ref BytesRead);
            if (BytesRead == BufferSize)
            {
                return BitConverter.ToInt32(ByteBuffer, 0);
            }
            else
            {
                throw new Exception("Failed to read memory");
            }
        }

        public static void WriteMemory(Int32 Address, byte[] Value)
        {
            int BytesWritten = 0;
            WriteProcessMemory((IntPtr)ProcessHandle, (IntPtr)Address, Value, Value.Length, ref BytesWritten);
            if (BytesWritten != Value.Length)
            {
                throw new Exception("Failed to write memory");
            }
        }

        public void ProcessSearcherProc()
        {
            while (true)
            {
                Process[] FoundProcesses = Process.GetProcessesByName("LethalLeague");

                if (FoundProcesses.Length != 0)
                {
                    if (!MainThread.IsAlive)
                    {
                        ProcessFound = true;
                        TargetProcess = FoundProcesses.First();

                        processFoundLbl.Invoke(new MethodInvoker(delegate
                        {
                            processFoundLbl.Text = "Found";
                            processFoundLbl.ForeColor = Color.Green;
                        }));

                        MainThread.Start();
                    }
                }
                else
                {
                    ProcessFound = false;
                    TargetProcess = null;
                    ProcessHandle = IntPtr.Zero;

                    processFoundLbl.Invoke(new MethodInvoker(delegate
                    {
                        processFoundLbl.Text = "Searching...";
                        processFoundLbl.ForeColor = Color.Black;
                    }));

                    if (MainThread.IsAlive)
                    {
                        MainThread.Abort();
                    }
                }
            }
        }

        public void MainProc()
        {
            const int PROCESS_VM_READ = 0x0010;
            const int PROCESS_VM_WRITE = 0x0020;
            const int PROCESS_VM_OPERATION = 0x0008;
            ProcessHandle = OpenProcess(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, false, TargetProcess.Id);
            Int32 BaseProcess = 0x0;

            foreach (ProcessModule Module in TargetProcess.Modules)
            {
                if (Module.ModuleName == "lethalleague.exe")
                {
                    BaseProcess = Module.BaseAddress.ToInt32();
                    break;
                }
            }
            

            while (ProcessFound)
            {
                Int32 PlayerFacingAddr, BallXAddr, BallYAddr;
                Int32 PlayerX, PlayerY, BallX, BallY;
                float BallWindup;

                try
                {
                    PlayerX = ReadMemory(DerefPointers(BaseProcess, new List<Int32>{ 0x001A9E54, 0x3C, 0xA4, 0x0, 0x10, 0x14, 0x18 }));
                    PlayerY = ReadMemory(DerefPointers(BaseProcess, new List<Int32> { 0x001A9E54, 0x3C, 0xA4, 0x0, 0x10, 0x14, 0x1C }));
                    BallXAddr = DerefPointers(BaseProcess, new List<Int32> { 0x001A9E54, 0x3C, 0xB4, 0x4, 0x10, 0x14, 0x18 });
                    BallX = ReadMemory(BallXAddr);
                    BallYAddr = DerefPointers(BaseProcess, new List<Int32> { 0x001A9E54, 0x3C, 0xB4, 0x4, 0x10, 0x14, 0x1C });
                    BallY = ReadMemory(BallYAddr);

                    byte[] BallWindupBuffer = BitConverter.GetBytes(ReadMemory(DerefPointers(BaseProcess, new List<Int32> { 0x001A9E54, 0x3C, 0x138, 0xA4, 0xD8, 0x52C })));
                    BallWindup = BitConverter.ToSingle(BallWindupBuffer, 0);

                    PlayerFacingAddr = DerefPointers(BaseProcess, new List<Int32> { 0x001A9E54, 0x3C, 0xA4, 0x4, 0x10, 0x2A0, 0x4 });

                    InGameLbl.Invoke(new MethodInvoker(delegate
                    {
                        InGameLbl.Text = "True";
                        BallWindupLbl.Text = BallWindup.ToString();
                    }));

                    PlayerX = NormalizeCoordinate(PlayerX);
                    PlayerY = NormalizeCoordinate(PlayerY);
                    BallX = NormalizeCoordinate(BallX);
                    BallY = NormalizeCoordinate(BallY);

                    int DistanceX = Math.Abs(PlayerX - BallX);
                    int DistanceY = Math.Abs(PlayerY - BallY);


                    if (!CarryBallChk.Checked)
                    {
                        // Ball within range
                        //if (DistanceX >= 0 && DistanceX <= BALL_RANGE_X &&
                          //  DistanceY >= 0 && DistanceY <= BALL_RANGE_Y)
                          if (Math.Pow(BallX - PlayerX, 2) + Math.Pow(BallY - PlayerY, 2) < Math.Pow(BALL_RANGE_RADIUS, 2))
                        {
                            //if ((PlayerX <= BallX && PlayerFacingRight) ||
                            //(PlayerX >= BallX && !PlayerFacingRight))


                            // Can hit the ball
                            if (BallWindup == 102.0f)
                            {
                                if (GetForegroundWindow() == TargetProcess.MainWindowHandle)
                                {
                                    // Press Z key
                                    SendKeys.SendWait("z");
                                }
                            }
                        }
                    }

                    // Correct player facing
                    if (AutoFaceChk.Checked)
                    {
                        Int32 NewPlayerFacing = 0;
                        if (BallX > PlayerX)
                            NewPlayerFacing = 1;
                        WriteMemory(PlayerFacingAddr, BitConverter.GetBytes(NewPlayerFacing));
                    }

                    // Carry ball
                    if (CarryBallChk.Checked)
                    {
                        BallX = PlayerX + 5;
                        BallY = PlayerY;
                        WriteMemory(BallXAddr, BitConverter.GetBytes(DenormalizeCoordinate(BallX)));
                        WriteMemory(BallYAddr, BitConverter.GetBytes(DenormalizeCoordinate(BallY)));
                    }
                }
                catch (Exception e)
                {
                    InGameLbl.Invoke(new MethodInvoker(delegate
                    {
                        InGameLbl.Text = "False";
                        BallWindupLbl.Text = "?";
                    }));
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            SearcherThread = new Thread(new ThreadStart(ProcessSearcherProc));
            MainThread = new Thread(new ThreadStart(MainProc));
            SearcherThread.Start();
        }

        
    }
}
